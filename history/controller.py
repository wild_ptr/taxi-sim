from __future__ import absolute_import
from __future__ import print_function
from ctypes import FormatError
from dataclasses import replace

import os
import sys
import optparse
# from tokenize import Double

# from pyrsistent import VT

from shapely.geometry import Polygon, LineString, Point
from sympy import li
import json

# we need to import python modules from the $SUMO_HOME/tools directory

if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")

from sumolib import checkBinary  # noqa
import traci  # noqa


import geopandas as gpd
import numpy as np

PREDTIME = 30.0 * 60
STEPTIME = 1 * 60

STOPFLAG = 3

FLEETSZ = 1000

class EnrouteInfo(object):

    def __init__(self, desArea:str, estArrivalTime:float) -> None:
        self._desArea = desArea                  # 最终到达区域（id）
        self._estArrivalTime = estArrivalTime    # 估计到达时间（s)
    
    @property
    def desArea(self):
        return self._desArea

    @property
    def estArrivalTime(self):
        return self._estArrivalTime


class RegionProjector(object):
    
    def __init__(self, regions) -> None:
        self._gdf =  gpd.read_file('data/taxi_zones/taxi_zones.shp')
        self._regions = set(regions)
        other_regions=[]
        for i in range(1,264):
            if i not in self._regions:
                other_regions.append(i)
        for i in other_regions:
            self._gdf.drop(i-1,inplace=True)
        self._gdf = self._gdf.to_crs('EPSG:4326')

    
    def inWhichRegion(self, x, y):
        series = self._gdf.contains(Point(x, y))
        inRegions = series[series == True]
        if len(inRegions) > 0:
            return inRegions.index[0] + 1   # region的编号比index大1
        else:
            return None
    
    @property
    def regionCentroids(self):
        return self._gdf.centroid
    
    @property
    def regionIDList(self): # 运营region集合
        return self._regions


class Decision(object):

    def __init__(self, service:dict, rebalance:dict) -> None:
        self._service = service
        self._rebalance = rebalance

    @property
    def service(self):
        return self._service

    @property
    def rebalance(self):
        return self._rebalance


class DecisionModel():

    def compute(self, vacantFleet:set, openReservations:set, regions:set) -> Decision:
        # np.random.seed(0)
        service = {}
        rebalance = {}
        openReservations = list(openReservations)
        regions = list(regions)
        
        assert len(regions) > 0
        for vID in vacantFleet:
            if len(openReservations) > 0:
                # flag = np.random.randint(0, 2)
                ### debug
                flag = 0
                if flag == 0:
                    # 随机匹配
                    resID = np.random.choice(openReservations, 1)[0]
                    openReservations.remove(resID)
                    service[vID] = resID 
                else:
                    # 随机指派到一个region
                    regionID = np.random.choice(regions, 1)[0]
                    rebalance[vID] = regionID
                    
            else:
                # 剩下的一半车辆再平衡
                # flag = np.random.randint(0, 2)
                ### debug
                flag = 0
                if flag == 0:
                    pass
                else:
                    # 随机指派到一个region
                    regionID = np.random.choice(regions, 1)[0]
                    rebalance[vID] = regionID
        
        return Decision(service, rebalance)


class Controller(object):

    def __init__(self) -> None:
        self._model = DecisionModel()
        self._servingPool = {}
        self._rebalancingPool = {}
        self._openReservationPool = {}  
        self._regionDemandRecord = {}
        self._centralEdges = {}
        self._errorReservationRec = {}


        # 区域中心点到边的映射
        with open('data/settings/centralEdges.json', 'r') as centralEdgesFile:
            self._centralEdges:dict = json.load(centralEdgesFile)
        old_keys = set(self._centralEdges.keys())
        for old_key in old_keys:
            new_key = int(old_key)
            self._centralEdges[new_key] = self._centralEdges[old_key]
            del self._centralEdges[old_key]

        self._regionProjector = RegionProjector(self._centralEdges.keys())

        # 初始化区域需求量记录
        self._regionDemandRecord = dict.fromkeys(self._regionProjector.regionIDList, 0)

        # 投放车辆
        vNumPerRegion = int(FLEETSZ/len(self._regionProjector.regionIDList))

        routes = dict.fromkeys(self._regionProjector.regionIDList)

        # 添加车队起始路线
        for regionID, edgeID in self._centralEdges.items():
            routeID = 'ROUTE' + str(regionID)
            traci.route.add(routeID=routeID, edges=[edgeID])
            routes[regionID] = routeID
        
        ### debug
        for regionID, routeID in routes.items():
            obj = traci.route.getEdges(routeID)
            print(obj)
        
        vCnt = 0
        # 往每个区域中心点添加车辆
        for regionID in self._regionProjector.regionIDList:
            routeID = routes[regionID]
            regionVehNum = vNumPerRegion
            while regionVehNum > 0:
                vCnt += 1
                assert vCnt <= FLEETSZ
                taxiID = 'TAXI' + str(vCnt)    
                traci.vehicle.add(vehID=taxiID, routeID=routeID, typeID='taxi', depart='now')
                regionVehNum -= 1
            
            # 剩下的车辆不足vNumPerRegion
            left = FLEETSZ - vCnt
            if left <= len(self._regionProjector.regionIDList):
                for regionIDj in self._regionProjector.regionIDList:
                    vCnt += 1
                    taxiID = 'TAXI' + str(vCnt)
                    routeID = routes[regionIDj]
                    traci.vehicle.add(vehID=taxiID, routeID=routeID, typeID='taxi', depart='now')
                    if vCnt == FLEETSZ:
                        break
        pass
        # self._regionProjector.regionIDList
        # self._centralEdges = dict.fromkeys(self._regionProjector.regionIDList)
        # centroids = self._regionProjector.regionCentroids
        # for i in centroids.keys():
        #     x = centroids[i].x
        #     y = centroids[i].y
        #     edge, _, _ =traci.simulation.convertRoad(x=x, y=y, isGeo=True)
        #     assert edge is not None
        #     self._centralEdges[i+1] = edge

        

    def step(self):
        ### 决策间隔为STEPTIME
        now = traci.simulation.getTime()
        if now % STEPTIME != 0:
            return 1
        ### 供给信息收集
        # 空车（包括vacant，rebalancing）集合
        emptyFleet = traci.vehicle.getTaxiFleet(0)
        # 将_servingPool中刚完成订单任务的车辆对应记录删除
        self._servingPool = { key: values for key, values in self._servingPool.items() 
                                if key not in emptyFleet }
        # 将_rebalancingPool中完成再平衡任务的车辆对应记录删除
        self._rebalancingPool = { key: values for key, values in self._rebalancingPool 
                                if traci.vehicle.getStopState(key) != STOPFLAG }
        
        # 删除emptyFleet中同时在_rebalancingPool里出现过的车辆ID删除
        emptyFleet = [vID for vID in emptyFleet if vID not in self._rebalancingPool.keys()]
        emptyFleet = set(emptyFleet)

        # 根据self._servingPool、self._rebalancingPool的信息，
        # 计算未来PREDTIME内每个region会产生的新供应量
        regionIDList = self._regionProjector.regionIDList
        newSupply = dict.fromkeys(regionIDList, 0)
 
        for vID, enrouteInfo in self._servingPool.items():
            if enrouteInfo.estArrivalTime - now <= PREDTIME:
                newSupply[enrouteInfo.desArea] += 1

        nowSupply = dict.fromkeys(regionIDList, 0)        

        ### 需求信息收集
        newReservations = traci.person.getTaxiReservations(1)
        # 将新到达的订单放入_openReservationPool
        for res in newReservations:
            assert res not in self._openReservationPool
            self._openReservationPool[res.id] = res
        
        for res in newReservations:
            pickupEdge = res.fromEdge
            pickupPos = res.departPos
            x, y = traci.simulation.convert2D(edgeID=pickupEdge, pos=pickupPos, toGeo=True)
            inRegion = self._regionProjector.inWhichRegion(x, y)
            assert inRegion is not None
            self._regionDemandRecord[inRegion] += 1
        
        # 调预测模块接口
        if now % PREDTIME == 0:
            # 重新训练
            pass
        else:
            # 不重新训练
            pass


        # 如果预测区间结束，_regionDemandRecord清零
        if now % PREDTIME == 0:
            self._regionDemandRecord = dict.fromkeys(regionIDList, 0) 


        ### 决策
        # decision = Decision()

        decision = self._model.compute(openReservations=self._openReservationPool.copy(), regions=self._regionProjector.regionIDList, vacantFleet=emptyFleet)

        ### 得到决策结果后
        # 再平衡
        for vID, regionID in decision.rebalance.items():
            actualEdge = self._centralEdges[regionID]

            # 控制车辆前往regionID对应的区域中心点
            stopData = traci.vehicle.getStops(vID)
            traci.vehicle.replaceStop(vID, 0, '')
            vEdgeID = traci.lane.getEdgeID(stopData[0].lane)
            vType = traci.vehicle.getTypeID(vID)
            routeStage = traci.simulation.findRoute(fromEdge=vEdgeID, toEdge=actualEdge, vType=vType)
            traci.vehicle.setRoute(vID, routeStage.edges)
            traci.vehicle.setStop(vID, flags=STOPFLAG, edgeID=actualEdge)

            # 记录预计到达时间
            estEnrouteTime = routeStage.travelTime
            self._rebalancingPool[vID] = EnrouteInfo(desArea=regionID, estArrivalTime=(now + estEnrouteTime))


        # 订单匹配
        for vID, resID in decision.service.items():
            
            
            
            # try:
            stops = traci.vehicle.getStops(vID)
            assert len(stops) == 1
            originalStopData = stops[0]
            originRoute = traci.vehicle.getRoute(vID)
            originLane, originEndPos, originStoppingPlaceID, originStopFlags, originDuration, originUntil = traci.vehicle.getNextStops(vID)[0]
            # vEdgeID = traci.lane.getEdgeID(stopData[0].lane)
            # vType = traci.vehicle.getTypeID(vID)
            resStage = self._openReservationPool[resID]

            # 判断订单起止点是否通，不通则将订单忽略
            vType = traci.vehicle.getTypeID(vehID=vID)
            resObj = self._openReservationPool[resID]
            
            ### 避免汽车进入死胡同（假定汽车原本不在dead-start也不在dead-end）
            # 检查订单起止点是否连通
            flag = None
            # 为避免重复计算起点终点的连通性
            if resID in self._errorReservationRec:
                # 此订单起终点的连通性检查过了
                flag, _ = self._errorReservationRec[resID]
                
            else:
                # 没检查过
                stgRes = traci.simulation.findRoute(fromEdge=resObj.fromEdge, toEdge=resObj.toEdge, vType=vType)
                stgRes_reverse = traci.simulation.findRoute(fromEdge=resObj.toEdge, toEdge=resObj.fromEdge, vType=vType)
                if len(stgRes.edges) == 0 or len(stgRes_reverse.edges) == 0:
                    # 汽车接不了这个订单或者接了铁定回不来
                    flag = True
                    self._errorReservationRec[resID] = (flag, self._openReservationPool.pop(resID))
                    continue
                else:
                    # 即使订单起止点相通，不代表汽车去接这个订单就能回来原来的位置，value代表(订单起止点“不通”（False）, 与订单起点不（互相）连通的边)
                    flag = False
                    self._errorReservationRec[resID] = (flag, set())
            assert flag is not None
            assert flag is False
            # 此订单起止点连通
            # 如果要执行这个订单，就要保证汽车完成订单之后可以回到原来的位置
            # 先看能不能接
            vehNowEdge = traci.lane.getEdgeID(originalStopData.lane)
            stgVeh = traci.simulation.findRoute(fromEdge=vehNowEdge, toEdge=resObj.fromEdge, vType=vType)
            stgVeh_reverse = traci.simulation.findRoute(fromEdge=resObj.fromEdge, toEdge=vehNowEdge, vType=vType)
            
            if len(stgVeh.edges) == 0 or len(stgVeh_reverse.edges) == 0:
                # a)接不了或b)能接但车回不来
                # 无论是情况a还是情况b，都不应该执行这个订单匹配指令
                # 记录vehNowEdge
                self._errorReservationRec[resID][1].add(vehNowEdge)
                continue
            else:
                # 能接且车能回来
                try:
                    traci.vehicle.dispatchTaxi(vID, [resID, resID])
                except Exception as ex:
                    print(ex)
                    exit('dispatchTaxi调用失败')
            

            
            
            # stgRes = traci.simulation.findRoute(fromEdge=resObj.fromEdge, toEdge=resObj.toEdge, vType=vType)
            # stgRes_reverse = traci.simulation.findRoute(fromEdge=resObj.toEdge, toEdge=resObj.fromEdge, vType=vType)


            # if len(stgRes.edges) == 0:
            #     # 订单起止点不通
            #     self._openReservationPool.pop(resID)
            #     self._errorReservationPool[resID] = 
            #     continue

            # if len(stgRes_reverse.edges) == 0:
            #     # 订单止起点不通
            #     self._deadEndReservationPool[resID] = self._openReservationPool[resID]
            # # print(stgRes)
            # stgVeh = traci.simulation.findRoute(fromEdge=traci.lane.getEdgeID(originalStopData.lane), toEdge=resObj.fromEdge, vType=vType)
            

            # if len(stgVeh.edges) > 0:
            #     # 车辆所在边与订单起点连通
            #     try:
            #         traci.vehicle.dispatchTaxi(vID, [resID, resID])
            #     except Exception as ex:
            #         print(vID + '和' + resID + '的匹配出现错误')
            #         print(ex)
            #         # dispatchTaxi()调用失败，Debug用的信息，看看怎么回事
            #         print('StopState:（dispatchTaxi前）')
            #         print(originalStopData)
            #         print(originRoute)
            #         print('StopState:（dispatchTaxi后）')
            #         # print(traci.vehicle.getStopState(vID))
            #         print('Stops:')
            #         print(traci.vehicle.getStops(vID))
            #         print(traci.vehicle.getRoute(vID))

            #         print('路线信息：')
            #         print('车到订单起点：')
            #         print(stgVeh)
            #         print('订单起点到终点：')
            #         print(stgRes)
            #         # print('Reservations:')
            #         # kkk = traci.person.getTaxiReservations(0)
            #         # print(traci.person.getTaxiReservations(0))

            #         # 去掉调用dispatchTaxi()导致的无效路线和stops
                    
            #         # traci.vehicle.setRoute(vID, originRoute)
            #         traci.vehicle.setRoute(vID, originRoute)
            #         print('setRoute后：')
            #         print(traci.vehicle.getStops(vID))
            #         print(traci.vehicle.getRoute(vID))

            #         vEdgeID = traci.lane.getEdgeID(originalStopData.lane)
            #         traci.vehicle.setStop(vehID=vID, edgeID=vEdgeID, pos=originalStopData.endPos, laneIndex=0, duration=originDuration, flags=originalStopData.stopFlags, startPos=originalStopData.startPos, until=originUntil)
            #         print('setStop后：')
            #         print(traci.vehicle.getStops(vID))
            #         print(traci.vehicle.getRoute(vID))
                    
            #         continue
            # 派单成功
            # 估算路程时间 
            edges = traci.vehicle.getRoute(vID)
            estEnrouteTime = 0
            for edge in edges:
                traveltime = traci.edge.getTraveltime(edgeID=edge)
                assert traveltime != -1
                estEnrouteTime += traveltime
            
            # 订单目的地的地理坐标(x, y)
            x, y = traci.simulation.convert2D(edgeID=resStage.toEdge, pos=resStage.arrivalPos, toGeo=True)

            # 记录预计到达时间
            regionID = self._regionProjector.inWhichRegion(x=x, y=y)
            self._servingPool[vID] = EnrouteInfo(desArea=regionID, estArrivalTime=(now + estEnrouteTime))

            # 从open订单池中删除相应订单记录
            self._openReservationPool.pop(resID)
                
        return 0





