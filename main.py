from __future__ import absolute_import
from __future__ import print_function

import os
import sys
import optparse

import packages  # 主要的代码在packages里

# we need to import python modules from the $SUMO_HOME/tools directory

if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")

from sumolib import checkBinary  # noqa
import traci  # noqa


sys.path.append("packages")


def run():
    """execute the TraCI control loop"""
    controller = packages.Controller()
    step = 0
    # 模拟进行4个小时
    packages.PoolingTaskInfo.init()
    while step < 15000:
        flag = controller.step()
        if flag == -1:
            break
        traci.simulationStep()
        step += 1
    traci.close()
    controller.finish()
    sys.stdout.flush()


def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
    options, args = optParser.parse_args()
    return options


# this is the main entry point of this script
if __name__ == "__main__":
    options = get_options()

    # this script has been called from the command line. It will start sumo as a
    # server, then connect and run
    if options.nogui:
        sumoBinary = checkBinary('sumo')
    else:
        sumoBinary = checkBinary('sumo-gui')

    # this is the normal way of using traci. sumo is started as a
    # subprocess and then the python script connects and runs
    traci.start(["sumo-gui", "-c", r"data/sumo_inputs/sumo.sumocfg", "--tripinfo-output.write-unfinished", "--tripinfo-output",
                 r"data/sumo_outputs/tripinfos.xml", "--stop-output", r"data/sumo_outputs/stopinfo.xml", '--vehroute-output', 
                 r'data/sumo_outputs/vehroute.rou.xml', "--fcd-output", r"data/sumo_outputs/fcd.xml", "--vehroute-output.exit-times", "--vehroute-output.write-unfinished"
                 , "--fcd-output.params", "device.taxi.state,device.taxi.customers,device.taxi.occupiedDistance,device.taxi.occupiedTime,device.taxi.currentCustomers"
                 , "--start"])
    # traci.start(["sumo-gui", "-c", r"data/sumo_inputs/sumo.sumocfg", "--tripinfo-output.write-unfinished", "--tripinfo-output",
    #              r"data/sumo_outputs/tripinfos.xml", "--stop-output", r"data/sumo_outputs/stopinfo.xml", '--vehroute-output', 
    #              r'data/sumo_outputs/vehroute.rou.xml', "--fcd-output", r"data/sumo_outputs/fcd.xml", "--vehroute-output.exit-times", "--vehroute-output.write-unfinished"
    #              ,"--start", "--fcd-output.params"])
    run()
