# 导包
from cgi import test
from hashlib import new
from itertools import count
from pkgutil import get_data
from statistics import mode
from unicodedata import name
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import datetime
from soupsieve import select
from sympy import false, true
import tensorflow
from statsmodels.tsa.stattools import adfuller
from sklearn.preprocessing import MinMaxScaler
from tensorflow import keras
from keras import callbacks
from torch import column_stack
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Conv2D, Flatten, Dense, LSTM, Dropout, GRU, Bidirectional
from tensorflow.keras.optimizers import SGD
from keras.models import load_model
import math
from sklearn.metrics import mean_squared_error

import warnings

# from test.demo1 import func
warnings.filterwarnings("ignore")

class predict_model():
    
    def __init__(self,file_path,model_name=GRU) -> None:
        super().__init__()
        self.file_path=file_path#获取文件路径
        self.model_name=model_name#默认GRU模型
        # self.region_list=[4,12]
        self.region_list = [ 12, 88, 13, 261, 87, 231, 209, 45, 125, 211, 144, 148, 232, 4, 79, 114, 113,
                  249, 158, 246, 68, 90, 234, 107, 224, 137, 170, 164, 186, 100, 50, 48, 230, 161, 162,163, 229,
                  140, 141, 237, 43, 142, 143, 239, 238, 236, 263, 262, 75, 151, 24,  74, 41,
                  166, 152, 42, 116, 244, 120, 243, 127, 128,233]
        self.region_list.sort()
        
    #读取数据
    def get_data(self):
        data=pd.read_csv(self.file_path)
        # data['datatime']=pd.to_datetime(data['datatime'])
        return data
    def data_pivot(self,columns,index):
        data=pd.read_csv(self.file_path)
        # data['datatime']=pd.to_datetime(data['datatime'])

        df_J=data.pivot(columns=columns, index=index)
        return df_J

    # 标准化函数
    def Normalize(self,df,col):
        average = df[col].mean()#取平均值
        stdev = df[col].std()#标准差
        df_normalized = (df[col] - average) / stdev#标准分数
        df_normalized = df_normalized.to_frame()
        return df_normalized, average, stdev

    # 差分化函数
    def Difference(self,df,col, interval):
        diff = []
        for i in range(interval, len(df)):
            value = df[col][i] - df[col][i - interval]
            diff.append(value)
        return diff
    
    #Target and Feature
    def TnF(self,df,steps=32):
        end_len = len(df)
        X = []
        y = []
        # steps = 32
        for i in range(steps, end_len):
            X.append(df[i - steps:i, 0])
            y.append(df[i, 0])
        X, y = np.array(X), np.array(y)
        return X ,y

    #fixing the shape of X_test and X_train
    def FeatureFixShape(self,train):
        train = np.reshape(train, (train.shape[0], train.shape[1], 1))
        # test = np.reshape(test, (test.shape[0],test.shape[1],1))
        return train
    #划分自变量x,因变量y
    def divide_x_y(self,data,location_id=4):
        # data=pd.read_csv(self.file_path)
        # data['datatime']=pd.to_datetime(data['datatime'])

        df_J=data.pivot(columns='location_id', index='datatime')
        # df_J =self.data_pivot("location_id", "datatime")
        df_4 = df_J[[('passenger_count', location_id)]]
        df_4.columns= df_4.columns.droplevel(level=0) 
        df_4=df_4[0:]
        # df_4.info()
        df_N4, av_J4, std_J4 = self.Normalize(df_4, location_id)
        Diff_4 = self.Difference(df_N4, col=location_id, interval=1) #taking an hour's diffrence
        df_N4 = df_N4[1:]
        df_N4.columns = ["Norm"]
        df_N4["Diff"]= Diff_4
        #Differencing created some NA values as we took a weeks data into consideration while difrencing
        df_J4 = df_N4["Diff"].dropna()
        df_J4 = df_J4.to_frame()
        #Assigning features and target 
        df_J4=df_J4.values.reshape(-1, 1)
        # df_J4
        X_trainJ4, y_trainJ4 = self.TnF(df_J4,12)
        # X_trainJ4
        X_trainJ4 = self.FeatureFixShape(X_trainJ4)
        x=[]
        x.append(df_J4[-12: ,0])
        x=np.array(x)
        x=np.reshape(x, (x.shape[0],x.shape[1], 1))
        recover4 = df_N4.Norm[-43:].to_frame()  #len as per the testset
        # recover4["Norm"][-1]    
        return X_trainJ4,y_trainJ4,x,av_J4,std_J4,recover4["Norm"][-1]  
    #Model for the prediction
    def GRU_model(self,X_Train, y_Train):
        early_stopping = callbacks.EarlyStopping(min_delta=0.001,patience=10, restore_best_weights=True) 
        #callback delta 0.01 may interrupt the learning, could eliminate this step, but meh!
        
        #The GRU model 
        model = Sequential()
        model.add(self.model_name(units=150, return_sequences=True, input_shape=(X_Train.shape[1],1), activation='tanh'))
        model.add(Dropout(0.2))
        model.add(self.model_name(units=150, return_sequences=True, input_shape=(X_Train.shape[1],1), activation='tanh'))
        model.add(Dropout(0.2))
        model.add(self.model_name(units=50, return_sequences=True, input_shape=(X_Train.shape[1],1), activation='tanh'))
        model.add(Dropout(0.2))
        model.add(self.model_name(units=50, return_sequences=True, input_shape=(X_Train.shape[1],1), activation='tanh'))
        model.add(Dropout(0.2))
        #model.add(GRU(units=50, return_sequences=True,  input_shape=(X_Train.shape[1],1),activation='tanh'))
        #model.add(Dropout(0.2))
        model.add(self.model_name(units=50, input_shape=(X_Train.shape[1],1), activation='tanh'))
        model.add(Dropout(0.2))
        model.add(Dense(units=1))
        #Compiling the model
        model.compile(optimizer=SGD(decay=1e-7, momentum=0.9),loss='mean_squared_error')
        model.fit(X_Train,y_Train, epochs=50, batch_size=150,callbacks=[early_stopping])
        # pred_GRU= model.predict(X_Test)
        return model
    def LSTM_model(self,X_Train,y_Train):
        pass
        
    #To calculate the root mean squred error in predictions
    def RMSE_Value(self,test,predicted):
        rmse = math.sqrt(mean_squared_error(test, predicted))
        print("The root mean squared error is {}.".format(rmse))
        return rmse
    def Unidimensional(self):
        #进行训练的区域数据
        
        x_list=[]
        y_list=[]
        test_list=[]
        av_list=[]
        std_list=[]
        recover_list=[]
        for i in self.region_list:
            x,y,x_test,av,std,recover=self.divide_x_y(pd.read_csv(self.file_path),i)
            x_list+=x.tolist()
            y_list+=y.tolist()
            test_list+=x_test.tolist()
            av_list.append(av)
            std_list.append(std)
            recover_list.append(recover)
        x_list=np.array(x_list)
        y_list=np.array(y_list)
        test_list=np.array(test_list)
        # x_list=model.FeatureFixShape(x_list)
        mo=self.GRU_model(x_list,y_list)
        return mo,test_list,av_list,std_list,recover_list
        # pass
    def Multidimensional(self):
        pass
        
    
    def save_model(self,model,path):
        model.save(path)
    def read_model(self,file):
        model=load_model(file)
        # return model
        return model
    def retrain(self,model,x_train,y_train):
        model.fit(x_train,y_train,batch_size=150,epochs=5)
        return model
    def add_data(self,file_path_,datatime,model_file,switch):
        data1=pd.read_csv(self.file_path)
        data2=pd.read_csv(file_path_)
        i1=datatime.split(' ')[1]
        hour=int(i1.split(':')[0])
        m=int(i1.split(':')[1])
        s=int(i1.split(':')[2])
        i_list=[]
        time_list=[]
        if m==30:
            i_list.append(datatime)
        for  j in self.region_list:
            time_list.append(datatime)
        for j in range(hour-16):
            i_list.append(f'2015-01-11 {17+j}:00:00')          
        for j in range(hour-17):
            i_list.append(f'2015-01-11 {17+j}:30:00')
        data2=data2.loc[data2.datatime.isin(i_list)]
        data_concat=pd.concat([data1,data2],axis=0)
        data_concat.reset_index(drop=True)
        x_list=[]
        y_list=[]
        test_list=[]
        av_list=[]
        std_list=[]
        recover_list=[]
        for i in self.region_list:
            x,y,x_test,av,std,recover=self.divide_x_y(data_concat,i)
            x_list.append(x[-1].tolist())
            y_list.append(y[-1].tolist())
            test_list+=x_test.tolist()
            av_list.append(av)
            std_list.append(std)
            recover_list.append(recover)
        x_list=np.array(x_list)
        y_list=np.array(y_list)
        test_list=np.array(test_list)
        # x_list=model.FeatureFixShape(x_list)
        old_model=self.read_model(model_file)
        mo=self.retrain(old_model,x_list,y_list)
        
        Pre=mo.predict(test_list)
        value_list=[]
        for i,x in enumerate(Pre):
            y=(x+recover_list[i])*std_list[i]+av_list[i]
            value_list.append(y[0])
        if switch==true:
            # pass
            mo.save(f'data/taxi_datas/model_.h5')
            df=pd.DataFrame()
            df['datatime']=time_list
            df['location_id']=self.region_list
            df['passenger_count']=value_list
            df.to_csv('data/taxi_datas/predict_output_.csv',index=0)
        return Pre,value_list
    def re_predict(self):
        pass
    def free_time(self,value,model_path,input_time='17:00:00'):
        hour=int(input_time.split(':')[0])
        minute=int(input_time.split(':')[1])
        second=int(input_time.split(':')[2])
        # pre_list=[]
        value_list=[]
        if minute>=0 and minute<30:
            new_pre,new_value=self.add_data('data/taxi_datas/predict_output_.csv',f'2015-01-11 {hour}:00:00',model_path,false)            
        else:
            minute=minute-30
            new_pre,new_value=self.add_data('data/taxi_datas/predict_output_.csv',f'2015-01-11 {hour}:30:00',model_path,false)
        
        
        return value,new_value
    def free(self,value,new_value,input_time='17:00:00'):
        hour=int(input_time.split(':')[0])
        minute=int(input_time.split(':')[1])
        second=int(input_time.split(':')[2])
        value_list=[]
        for i,x in enumerate(self.region_list):  
            time_second=minute*30+second     
            # pre_list.append(pre[i]*(time_second)/1800+new_pre[i]*(1800-time_second)/1800)
            value_list.append(value[i]*time_second/1800+new_value[i]*(1800-time_second)/1800)
        return value_list
    def func(self,):
        mo,test_list,av_list,std_list,recover_list=self.Unidimensional()
        # Pre=[mo.predict(i) for i in test_list]
        mo.save('data/taxi_datas/model.h5')
        Pre=mo.predict(test_list)
        value_list=[]
        for i,x in enumerate(Pre):
            y=(x+recover_list[i])*std_list[i]+av_list[i]
            value_list.append(y[0])
        time_list=[]
        count_list=value_list
        for i in self.region_list:
            time_list.append('2015-01-11 17:00:00')
        df=pd.DataFrame()
        df['datatime']=time_list
        df['location_id']=self.region_list
        df['passenger_count']=value_list
        df.to_csv('data/taxi_datas/predict_output.csv',index=0)
        return Pre,value_list
        # return df_N4
if __name__=='__main__':

    model=predict_model('data/taxi_datas/14-12-15-01.csv')
    pre,value_list=model.func()#第一次训练
    # new_pre,new_value=model.add_data('./2015-01-11_17-21.csv',['2015-01-11 17:00:00'],'./test/model.h5')#添加真实值第二次训练
    # print(pre,value_list)
    # print(new_pre,new_value)
    # pre3,value3=model.free_time(pre,value_list,'./test/model.h5','17:05')#不添加真实值第二次训练
    print(pre,value_list)
    # print(pre3,value3)