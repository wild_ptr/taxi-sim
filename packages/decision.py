from cmath import inf
from ortools.linear_solver import pywraplp
import numpy as np
from ortools.sat.python import cp_model
# from hungarian_algorithm import algorithm as hungarian_algorithm

from munkres import Munkres
import time
# from ortools.graph import pywrapgraph
from ortools.graph.python import min_cost_flow
import copy

class Model(object):

    def __init__(self, vrdistPenalty, timeReward, matchReward) -> None:
        self._vrdistPenalty = vrdistPenalty
        self._timeReward = timeReward
        self._matchReward = matchReward

    def decide(self, vNum, resNum, distanceVRLst, elapsedWaitTimeLst):
        solver = pywraplp.Solver.CreateSolver('CBC')
        data = {}
        data['num_res'] = resNum
        data['num_vehicle'] = vNum
        data['obj_coeffs_vrdist'] = distanceVRLst
        data['obj_coeffs_elawaittime'] = elapsedWaitTimeLst
        data['constraint_coeffs_1res1v'] = [[1] * vNum for _ in range(resNum)]
        data['constraint_coeffs_1v1res'] = [[1] * resNum for _ in range(vNum)]
        data['bounds_1res1v'] = [1] * resNum
        data['bounds_1v1res'] = [1] * vNum
        data['obj_coeffs_matchreward'] = self._matchReward
        data['obj_coeffs_timereward'] = self._timeReward
        data['obj_coeffs_vrdistpenalty'] = self._vrdistPenalty

        # infinity = solver.infinity()
        matchVars = []
        ### 决策变量
        for i in range(data['num_res']):
            irow = []
            matchVars.append(irow)
            for j in range(data['num_vehicle']):
                irow.append(solver.BoolVar(f'matchVars[%d][%d]' % (i,j)))


        ### 目标函数
        objective = solver.Objective()
        # # 接单导致的成本
        # for i in range(data['num_res']):
        #     for j in range(data['num_vehicle']):
        #         objective.SetCoefficient(matchVars[i][j], data['obj_coeffs_vrdist'][i][j] * data['obj_coeffs_vrdistpenalty'])

        # # 接单导致的奖励（等待时间）
        # for i in range(data['num_res']):
        #     iWaitTime = data['obj_coeffs_elawaittime'][i]
        #     for j in range(data['num_vehicle']):
        #         objective.SetCoefficient(matchVars[i][j], (-iWaitTime) * data['obj_coeffs_timereward'])
        
        # # 接单导致的奖励（接单成功）
        # for i in range(data['num_res']): 
        #     for j in range(data['num_vehicle']):
        #         objective.SetCoefficient(matchVars[i][j], -data['obj_coeffs_matchreward'])

        # 修改：
        for i in range(data['num_res']): 
            iWaitTime = data['obj_coeffs_elawaittime'][i]
            for j in range(data['num_vehicle']):
                objective.SetCoefficient(matchVars[i][j], data['obj_coeffs_vrdist'][i][j] * data['obj_coeffs_vrdistpenalty']
                 -iWaitTime * data['obj_coeffs_timereward'] - data['obj_coeffs_matchreward'])


        objective.SetMinimization()
        
        ### 限制
        for i in range(data['num_res']):
            constraint = solver.RowConstraint(0, data['bounds_1res1v'][i], '')
            for j in range(data['num_vehicle']):
                constraint.SetCoefficient(matchVars[i][j], data['constraint_coeffs_1res1v'][i][j])
        
        for j in range(data['num_vehicle']):
            constraint = solver.RowConstraint(0, data['bounds_1v1res'][j], '')
            for i in range(data['num_res']):
                constraint.SetCoefficient(matchVars[i][j], data['constraint_coeffs_1v1res'][j][i])

        status = solver.Solve()

        if status == pywraplp.Solver.OPTIMAL:
            print('Objective value =', solver.Objective().Value())
            print('Decision variables:')
            # for i in range(data['num_res']):
            #     for j in range(data['num_vehicle']):
            #         print(matchVars[i][j].name(), ' = ', matchVars[i][j].solution_value(), end='\t')
            #     print()
            print('...')
            print('Problem solved in %f milliseconds' % solver.wall_time())
            print('Problem solved in %d iterations' % solver.iterations())
            print('Problem solved in %d branch-and-bound nodes' % solver.nodes())
            result = []
            for i in range(data['num_res']):
                row = []
                for j in range(data['num_vehicle']):
                    row.append(matchVars[i][j].solution_value())
                result.append(row)
            return result
        else:
            print('The problem does not have an optimal solution.')
            return None    

class Model2(object):

    def __init__(self, vrdistPenalty, timeReward, matchReward) -> None:
        self._vrdistPenalty = vrdistPenalty
        self._timeReward = timeReward
        self._matchReward = matchReward

    def decide(self, vNum, resNum, distanceVRLst, elapsedWaitTimeLst):
        model = cp_model.CpModel()
        data = {}
        data['num_res'] = resNum
        data['num_vehicle'] = vNum
        data['obj_coeffs_vrdist'] = distanceVRLst
        data['obj_coeffs_elawaittime'] = elapsedWaitTimeLst
        data['constraint_coeffs_1res1v'] = [[1] * vNum for _ in range(resNum)]
        data['constraint_coeffs_1v1res'] = [[1] * resNum for _ in range(vNum)]
        data['bounds_1res1v'] = [1] * resNum
        data['bounds_1v1res'] = [1] * vNum
        data['obj_coeffs_matchreward'] = self._matchReward
        data['obj_coeffs_timereward'] = self._timeReward
        data['obj_coeffs_vrdistpenalty'] = self._vrdistPenalty


        matchVars = []
        ### 决策变量
        for i in range(data['num_res']):
            irow = []
            matchVars.append(irow)
            for j in range(data['num_vehicle']):
                irow.append(model.NewBoolVar(f'matchVars[%d][%d]' % (i,j)))
        
        ### 限制
        # 一个订单最多由一辆车执行
        for i in range(data['num_res']):
            model.Add(sum(matchVars[i][j] for j in range(data['num_vehicle'])) <= 1)

        # 一辆车最多执行一个订单
        for j in range(data['num_vehicle']):
            model.Add(sum(matchVars[i][j] for i in range(data['num_res'])) <= 1)

        ### 目标函数
        objective_terms = []
        for i in range(data['num_res']):
            for j in range(data['num_vehicle']):
                costij = data['obj_coeffs_vrdist'][i][j] * data['obj_coeffs_vrdistpenalty'] - data['obj_coeffs_elawaittime'][i] * data['obj_coeffs_timereward'] - data['obj_coeffs_matchreward']
                objective_terms.append(costij * matchVars[i][j])

        model.Minimize(sum(objective_terms))

        solver = cp_model.CpSolver()
        status = solver.Solve(model)

        if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
            print(f'Total cost = {solver.ObjectiveValue()}')
            print()
            result = []
            for i in range(data['num_res']):
                row = []
                for j in range(data['num_vehicle']):
                    row.append(solver.BooleanValue(matchVars[i][j]))
                result.append(row)
            print('Problem solved in %f seconds' % solver.UserTime())
            return result
        else:
            print('No solution found.')
            return None
        

class RebalanceModel(object):

    def decide(self, now, regionalCosts, regionSupplyRates, regionResArrivalRates):
        mapper = list(regionSupplyRates.keys())
        data = {}
        data['num_region'] = len(mapper)
        data['obj_coeffs_regionalCost'] = regionalCosts
        data['constants_regionSupplyRates'] = regionSupplyRates
        data['constants_regionResArrivalRates'] = regionResArrivalRates
        data['threshold'] = 0

        solver = pywraplp.Solver.CreateSolver('GLOP')
        
        # 定义决策变量
        flowVars = []
        for i in range(data['num_region']):
            row  = []
            for j in range(data['num_region']):
                row.append(solver.NumVar(0, solver.infinity(), f'r_%d_%d'%(i,j)))
            flowVars.append(row)
        
        
        if sum(data['constants_regionSupplyRates'].values()) >= sum(data['constants_regionResArrivalRates'].values()):
            # 总供给大于等于总需求的情况
            # 定义约束
            for i in range(data['num_region']):
                row = [var for idx, var in enumerate(flowVars[i]) if idx != i]
                col = []
                for j in range(data['num_region']):
                    if j != i:
                        col.append(flowVars[j][i])
                # 之前写错了，写成（+ sum(row) - sum(col)）
                solver.Add(data['constants_regionSupplyRates'][mapper[i]] - sum(row) + sum(col) - data['constants_regionResArrivalRates'][mapper[i]] >= data['threshold'])
            
        else:
            # 总供给小于总需求的情况
            # 定义约束
            for i in range(data['num_region']):
                row = [var for idx, var in enumerate(flowVars[i]) if idx != i]
                col = []
                for j in range(data['num_region']):
                    if j != i:
                        col.append(flowVars[j][i])
                # 之前写错了，写成（+ sum(row) - sum(col)）
                solver.Add(data['constants_regionSupplyRates'][mapper[i]] - sum(row) + sum(col) - data['constants_regionResArrivalRates'][mapper[i]] <= 0)
                
                # bounds = (sum(data['constants_regionSupplyRates'].values()) - sum(data['constants_regionResArrivalRates'].values())) / data['num_region']
                # solver.Add(data['constants_regionSupplyRates'][mapper[i]] + sum(row) - sum(col) - data['constants_regionResArrivalRates'][mapper[i]] <= bounds)
                # 已有+流入 >= 流出
                # solver.Add(data['constants_regionSupplyRates'][mapper[i]] + sum(row) - sum(col) >= 0)
            # 定义目标函数
            # 两次规划求解
            # 第一次最小化供需差
            # 可能要设置阈值
            # solver2 = pywraplp.Solver.CreateSolver('GLOP')
        for i in range(data['num_region']):
            solver.Add(flowVars[i][i] == 0)
        # 定义目标函数
        solver.Minimize(sum(flowVars[i][j] * data['obj_coeffs_regionalCost'][(mapper[i], mapper[j])] for i in range(data['num_region']) for j in range(data['num_region']) if i != j))


            # # 第二次最小化调度成本
            # solver.Minimize(sum)
        
        status = solver.Solve()

        if status == pywraplp.Solver.OPTIMAL:
            print('Solution:')
            print('Objective value =', solver.Objective().Value())
            # print('x =', x.solution_value())
            # print('y =', y.solution_value())
            resultFlows = {}
            for i in range(data['num_region']):
                for j in range(data['num_region']):
                    if i != j:
                        fromRegionID = mapper[i]
                        toRegionID = mapper[j]
                        value = flowVars[i][j].solution_value()
                        resultFlows[(fromRegionID, toRegionID)] = value
            return resultFlows
        else:
            print('The problem does not have an optimal solution.')
            return None


class RebalanceModel2(object):
    
    def decide():
        pass
            
class DispatchModel(object):
    def __init__(self) -> None:
        self._m = Munkres()


    def decideHungarian(self, workerLst:list, taskLst:list, costMatrix:list) -> list:
        # begin = time.time()
        indexes = self._m.compute(costMatrix)
        # end = time.time()
        # print(f'司乘匹配算法用时：%f' % (end - begin))
        returnResult = {}
        for tup in indexes:
            workerName = workerLst[tup[0]]
            taskName = taskLst[tup[1]]
            returnResult[workerName] = taskName
        return returnResult
        
    
    def decideMinFlowCost(self, workerLst:list, taskLst:list, costMatrix:list) -> list:
        # 准备solver的input
        self._min_cost_flow = min_cost_flow.SimpleMinCostFlow()
        numWorker = len(workerLst)
        numTask = len(taskLst)
        isLegal = False
        
        if numTask > numWorker:
            # worker不足的情况
            fakeWorkerNum = numTask - numWorker
            fakeTaskNum = -1
            finalNum = numTask  # 两边图其中一边的节点数
        elif numTask == numWorker:
            fakeWorkerNum = -1
            fakeTaskNum = -1
            finalNum = numTask  # 两边图其中一边的节点数
        else:
            # worker有剩余的情况
            fakeWorkerNum = -1
            fakeTaskNum = numWorker - numTask
            finalNum = numWorker  # 两边图其中一边的节点数

        # 构造source和worker之间的边
        start_nodes = [0 for _ in range(finalNum)]
        for i in range(1, finalNum + 1):
            start_nodes += [i for _ in range(finalNum)]
        start_nodes += [i for i in range(finalNum + 1, finalNum + finalNum + 1)]
        
        # 构造worker和task之间的边
        end_nodes = [i for i in range(1, finalNum + 1)]
        for _ in range(finalNum):
            tempLst = [k for k in range(finalNum + 1, finalNum + finalNum + 1)]
            end_nodes += tempLst
        end_nodes += [finalNum + finalNum + 1 for _ in range(finalNum)] 

        capacities = [1 for _ in range(finalNum)] + [1 for _ in 
                        range(finalNum * finalNum)] + [1 for _ in range(finalNum)] 

        
        newCostMatrix = copy.deepcopy(costMatrix)
        # 拓展成本矩阵
        if numTask > numWorker:
            for i in range(fakeWorkerNum):
                workerCost = [0 for _ in range(finalNum)]
                newCostMatrix.append(workerCost)
        if numTask < numWorker:
            for lst in newCostMatrix:
                lst += [0 for _ in range(fakeTaskNum)]
        
        costs = [0 for _ in range(finalNum)]
        for i in range(finalNum):
            costs += newCostMatrix[i]
        costs += [0 for _ in range(finalNum)]
        # costs = (costs)

        source = 0
        sink = finalNum + finalNum + 1
        tasks = finalNum
        supplies = [tasks] + [0 for _ in range(2 * finalNum)] + [-tasks]
        
        # Add each arc.
        for i in range(len(start_nodes)):
            self._min_cost_flow.add_arcs_with_capacity_and_unit_cost(start_nodes[i],
                                                        end_nodes[i], capacities[i],
                                                        costs[i])
        # Add node supplies.
        for i in range(len(supplies)):
            self._min_cost_flow.set_nodes_supplies(i, supplies[i])

        status = self._min_cost_flow.solve()
        isLegal = True


        result = {}
        if isLegal is True:
            if status == self._min_cost_flow.OPTIMAL:
                print('Total cost = ', self._min_cost_flow.optimal_cost())
                print()
                for arc in range(self._min_cost_flow.NumArcs()):
                    # Can ignore arcs leading out of source or into sink.
                    if self._min_cost_flow.tail(arc) != source and self._min_cost_flow.head(
                            arc) != sink and (self._min_cost_flow.tail(arc) <= numWorker 
                            and self._min_cost_flow.head(arc) <= max(numTask, numWorker) + numTask):

                        # Arcs in the solution have a flow value of 1. Their start and end nodes
                        # give an assignment of worker to task.
                        if self._min_cost_flow.Flow(arc) > 0 and self._min_cost_flow.UnitCost(arc) > 0:
                            print('Worker %d assigned to task %d.  Cost = %d' %
                                (self._min_cost_flow.tail(arc), self._min_cost_flow.head(arc),
                                self._min_cost_flow.UnitCost(arc)))
                            
                            tail = self._min_cost_flow.tail(arc)
                            head = self._min_cost_flow.head(arc)

                            result[workerLst[tail - 1 ]] = taskLst[head - max(numTask, numWorker) - 1]
                        
            else:
                print('There was an issue with the min cost flow input.')
                print(f'Status: {status}')
                raise Exception('DispatchModel')
        else:
            raise Exception('DispatchModel')
        
        return result

    def decideMinFlowCostNew(self, workerLst:list, taskLst:list, costMatrix:list):
        workerMapper = {}
        taskMapper = {}
        # 加上虚拟节点之后的工人数（任务数）
        finalNum = max(len(workerLst), len(taskLst))
        workerNodes = list(range(finalNum))
        taskNodes = list(range(finalNum, 2 * finalNum))
        # 创建从节点编号到原编号的映射
        for nodeNum, initName in zip(workerNodes, workerLst):
            workerMapper[nodeNum] = initName
        
        for nodeNum, initName in zip(taskNodes, taskLst):
            taskMapper[nodeNum] = initName
        
        costMatrixNP = np.array(costMatrix)
        # 补充矩阵（加0）
        if len(workerLst) < finalNum:
            costMatrixNP = np.append(costMatrixNP, np.zeros( ( finalNum-len(workerLst), finalNum ) ), axis=0)
        if len(taskLst) < finalNum:
            costMatrixNP = np.append(costMatrixNP, np.zeros( ( finalNum, finalNum - len(taskLst) ) ), axis=1)
        
        # 构建startNodes
        startNodes = []
        for nodeNum in workerNodes:
            startNodes += [nodeNum] * finalNum
        
        # 构建endNodes
        endNodes = []
        for _ in range(finalNum):
            endNodes += taskNodes
        
        # 构建capacities
        capacities = [1] * (finalNum * finalNum)

        # 构建unitCosts
        unitCosts = costMatrixNP.flatten()

        # 构建supplies
        supplies = ( [1] * finalNum ) + ( [-1] * finalNum )

        # 转换成numpy的array
        startNodes = np.array(startNodes)
        endNodes = np.array(endNodes)
        capacities = np.array(capacities)
        supplies = np.array(supplies) 
        
        # Instantiate a SimpleMinCostFlow solver.
        smcf = min_cost_flow.SimpleMinCostFlow()

        allArcs = smcf.add_arcs_with_capacity_and_unit_cost(
            startNodes, endNodes, capacities, unitCosts
        )

        smcf.set_nodes_supplies(np.arange(0, len(supplies)), supplies)
        # Find the min cost flow.
        status = smcf.solve()

        if status != smcf.OPTIMAL:
            print('最低费用流求解出现问题，本次决策不进行指派')
            return  {}
        # print(f"Minimum cost: {smcf.optimal_cost()}")
        # print("")
        # print(" Arc    Flow / Capacity Cost")
        solution_flows = smcf.flows(allArcs)
        costs = solution_flows * unitCosts
        # for arc, flow, cost in zip(allArcs, solution_flows, costs):
        #     print(
        #         f"{smcf.tail(arc):1} -> {smcf.head(arc)}  {flow:3}  / {smcf.capacity(arc):3}       {cost}"
        #     )
        res = {}
        for arc, flow, cost in zip(allArcs, solution_flows, costs):
            tail = smcf.tail(arc)
            head = smcf.head(arc)
            # 不涉及虚拟节点
            if flow == 1 and tail in workerMapper and head in taskMapper:
                workerInitName = workerMapper[tail]
                taskInitName = taskMapper[head]
                res[workerInitName] = taskInitName

        return res

        


if __name__ == "__main__":
    model = DispatchModel()

    # wlst = ['A', 'B', 'C']
    # tlst = ['a', 'b', 'c']
    # C = [ [4, 1, 9],
    #       [2, 3, 2],
    #       [7, 2, 3]
    #     ]
    # 无限：
    # float('inf')
    # wlst = ['A', 'B', 'C']
    # tlst = ['a', 'b']
    # C = [ [3, 1,],
    #       [2, 4,],
    #       [7, 5,]
    #     ]

    wlst = ['A', 'B']
    tlst = ['a', 'b', 'c']
    C = [ [4, 1, 9],
          [2, 3, 2],
        ]    

    for i in range(-1):
        print()

    print(model.decideMinFlowCostNew(wlst, tlst, C))

    print(model.decideHungarian(wlst, tlst, C))
    # print(result)




    
            


