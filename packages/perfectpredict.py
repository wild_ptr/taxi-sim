import pandas as pd
import geopandas as gpd
from shapely.geometry import Point



class PerfectPred(object):

    def __init__(self) -> None:
        self.data = pd.read_csv(r'data/pre_cal/24hour_end_1.csv')   
        self.region_list=[12,88,87,261,13,209,45,231,125,211,144,148,232,4,79,113,114,249,158]
        # self.region_list.sort()
        geodata = gpd.read_file('data/taxi_zones/taxi_zones.shp')
        geodata=geodata.loc[geodata.borough=='Manhattan']
        geodata=geodata.loc[geodata.LocationID.isin(self.region_list)]
        self.dataWGS84 = geodata.to_crs('EPSG:4326')
        del geodata

    def getPrediction(self,now,span):
        span=now+span
        df=self.data.loc[(self.data['pickup_datetime']>=now)&(self.data['pickup_datetime']<span)]
        point_list=[Point(df.pickup_longitude[i],df.pickup_latitude[i])  for i in df.index.values]
        
        id_list=[self.dataWGS84.contains(i)[self.dataWGS84.contains(i) == True].index[0]+1 for i in point_list]
        # print(id_list)
        count_list=[id_list.count(i) for i in self.region_list]
        demand_list=dict(zip(self.region_list,count_list))
        del df,point_list,id_list,count_list
        return demand_list

if __name__ == "__main__":
    pp = PerfectPred()
    obj = pp.getPrediction(0, 60 * 15)
    # obj=pp.span_time(0,10*60)
    print(obj)



        