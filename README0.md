![](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/screenshot.png)

### 简介

基于SUMO的TraCI接口实现的出租车拼车业务仿真模拟。

### 运行

- 从[官网](https://sumo.dlr.de/docs/Downloads.php)下载SUMO，并依据[官方文档](https://sumo.dlr.de/docs/Installing/index.html)指引设置好相应的环境变量。

- 克隆本仓库

- 进入仓库的根目录，在命令行输入命令：

  ```
  python main.py
  ```

  即可运行。

### 文件结构

```
.
├── data
│   ├── mylogs
│   ├── pre_cal
│   ├── settings
│   ├── sumo_inputs
│   ├── sumo_outputs
│   ├── taxi_datas
│   ├── taxi_zones
│   └── temp
│       └── rou
├── docs
├── history
└── packages
```

- data目录用于放置
  - 场景输入文件：与sumo进程的启动配置文件、模拟需要的输入文件（路网、车辆、人等）、以及其他预设文件
  - log文件
  - 临时文件
- packages目录用于放置本python工程的代码
- docs目录用于放置项目过程中用以总结的文档

### 目录

- [研究目的](#研究目的)

- [数据来源](#数据来源)

- [数据清洗与转换](#数据清洗与转换)

- [仿真环境搭建](#仿真环境搭建)

- [决策模型/算法](#决策模型/算法)

- [仿真数据分析](#仿真数据分析)
- [参考文献](#参考文献)

### 研究目的

通过搭建一个基于真实出行数据的出租车模拟仿真应用，以满足目前拼车算法和其他相关算法的研究工作。

### 数据来源

![](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/2-1.png)

<p align="center">图1 订单数量分布总览情况</p>

纽约市开放数据提供从2009年至2016年的出租车运营数据，包含超过10亿辆黄色和绿色出租车的载客数据。数据集共包含19个维度，我们保留了其中行程相关的关键维度共5个： pickup_datetime、pickup_longitude、pickup_latitude、dropoff_longitude、dropoff_latitude。其中pickup_datetime代表的是订单的发起时间，pickup_longitude、pickup_latitude分别代表订单的上车地点的经纬度信息，dropoff_longitude、dropoff_latitude分别代表订单的下车点的经纬度信息。通过对纽约市一周数据进行初步分析，由图1可知，乘客在7:00与18：00左右的出行需求非常高，这两个时间段正是上下班早晚高峰期，其出行分别符合实际，使用这样的数据集生成最终的出行需求文件让我们研究的模拟仿真平台更具有真实性。

数据集链接：[TLC Trip Record Data - TLC (nyc.gov)](https://www1.nyc.gov/site/tlc/about/tlc-trip-record-data.page)

### 数据清洗与转换

![](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/screenshot2.png)

<p align="center">图2 曼哈顿部分区域地图</p>

通过sumolib中提供的库函数convertRoad()，我们将订单的上/下车位置坐标映射到路网文件的边上。在仿真测试过程中，我们发现路网中的“死胡同”会使得一些订单无法执行，于是我们将起止点没有通路的订单去除掉。另外，为了减少仿真时间，我们只截取了曼哈顿的一部分区域。

### 仿真环境搭建

![3-1](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/3-1.png)

<p align="center">图3 整体框架图</p>

本项目的仿真环境基于SUMO的提供的TraCI (**Tra**ffic **C**ontrol **I**nterface) API开发。TraCI的工作原理如下：sumo进程作为**服务端**，完成启动并载入场景数据后，将开启一个端口开始监听。外部的**客户端**sumo进程与服务端完成连接后，直到仿真结束，负责控制sumo的每一步模拟的执行。通过这种进程间通信的方式，客户端一方面可以取得模拟环境中的绝大部分数据，另一方面，还可以修改其中的部分变量[1]。如图，本项目的仿真环境主要由三部分组成：1)**场景数据输入**，2)**交通控制脚本**，3)**仿真运行日志数据输出**。

### 决策模型/算法

项目目前实施了三种决策算法/模型：

#### 基准模型——典型的指派问题优化

![3-4](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/3-4.png)

<p align="center">图4 指派问题</p>

和标准的指派问题类似，每辆车一次只能接一个订单，但是和标准指派不一样的地方是，工人数（空闲车辆数）和任务数（open订单数）往往是不等的，这就需要在实际求解的时候引入虚拟工人或者虚拟任务，使得总工人数和总任务数相等。在求解算法方面我们起初尝试了匈牙利算法，但是求解效率比较低。为了提高基准模型的求解速度，我们采取了最小成本流的方式求解，在大多数情况下，速度提高了好几倍。具体采用的是谷歌Ortools软件包中的一个专门求解最小成本流问题的Solver。

#### 拼车算法——“启发式插入算法”

本算法的思想是，将在最小化插入后引起其他任务点（上车点、下车点）的完成时间的延迟+被插入订单的上下车点完成时间的前提下，满足尽量多的订单。算法具体步骤见[2]。

#### 平衡调度模型——基于车流量和供需预测值的线性规划模型

受[3]所提出的模型启发，我们建立了一个线性规划模型，目标为最小化平衡调度所引起的总时间成本，约束为保证各个区域在未来一段时间窗内，预计供给量大于等于预计需求量。

接下来，我们通过一系列数据实验，以基准模型为对照组，分析后两个算法/模型的效果。

### 仿真数据分析

#### 订单数据说明

在此项目中，我们截取纽约市曼哈顿下面的一点小区块，大大减少了数据的量级和降低了算法的复杂度，同时将调度算法由于跨多区域调度，而导致的调度时间过长问题有效地解决。所以我们截取纽约市2014年1月7号这一天某段时间的数据进行模拟仿真。

![](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/4-3.png)

<p align="center">图5 上下车点热力分布图</p>

图5所表示的是我们选取模拟区域的上下车点分布图像，其中蓝色表示上车点，红色表示下车点，可见乘客一般会在主干道上车，在相对于主干道垂直街道的下车。

#### 一对一的出租车模式与一对多的拼车模式对比分析

![](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/4-6.png)

<p align="center">图6 不同规模下的效果</p>

通过图6，我们看到再有拼车和没拼车对比之下，明显可以看出有拼车的平均等待时间和平均路由长度都是比没拼车的情况下小的。

![](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/4-13.png)

<p align="center">图7 每5分钟的平均等待时间</p>

我们对于拼车模型和基础模型进行对比分析。

通过观察图7，我们可以看出，在使用拼车模型之后，顾客的等待时间大幅度降低，而且相对于基础模型来说，变化也比较缓慢，所以我们初步可以判断拼车模型还是相对比较更优的。

![](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/4-14.png)

<p align="center">图8 每5分钟累计完成订单量</p>

在图8展示了分析每5分钟的累计完成订单量，对于基础模型和拼车模型来说相比并不是很明显，猜测是由于，拼车造成原本订单路径的改变，导致路程稍微增加，时间稍微增加，虽然对于等待时间地降低，但是对于整体完成效率地提升不大。

![](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/4-15.png)

<p align="center">图9 一段时间内承载人数车辆的占比</p>

在图9中，我们看到，对于300辆车地规模，拼车的数量总共占约4成，相对来说还是比较优的，但对于3人或4人一同乘车的情况下还是比较少量的，这是由于，我们这个并不是完全和真实情况一致，召之即来，所以我们会判断在此情况下是否对车上乘客产生较大地影响。可能对于较小规模下，拼车的效果更佳。

![](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/4-16.png)

<p align="center">图10 一段时间内承载人数车辆的占比</p>

在图10中，我们调整了一辆车地最大载客数量，设置为10，这是相当于一辆小巴士的级别。观察后可以知道，最大载客数量调整之后，并没有使得车辆拼车情况大幅增加，应该是由于我们车队规模较大，订单完成较快。如果车队规模较小，我们的订单就会累计下来，使得顾客等待时间增加，所以凡是都有两面性。我们研究地大方向还是尽可能地满足客户地需求。



通过数据观察，我们发现通过拼车算法可以使得多个乘客的路线有所重合，从而最终完成所需订单的行驶距离D会有所下降。Moeid Qurashi, Hai Jiang等人认为TVKT——因拼车而减少的车辆总行驶公里数是衡量拼车算法优劣的重要指标[4]。

由提供的同时为了通过确定模拟仿真平台的较好的车队规模，我们从其减少的车辆总行驶公里数TVKT占订单距离的比例出发，判断出较好的车队规模进行后续分析。

![](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/4-17.png)

<p align="center">图11 同车队规模下D与TVKT对比分析</p>

通过分析可知，随着车队规模的提升，车辆行驶总路程D也逐渐增加，说明同一时间段内更多的订单得到满足，此时因拼车而减少的车辆路程TVKT也是在逐渐增加的。

<p align="center">表1 不同车队规模下的距离减少程度</p>

| FleetSize | TVKT     | D        | SD       | TVKT/D |
| --------- | -------- | -------- | -------- | ------ |
| 200       | 257189.3 | 10046458 | 9789269  | 0.0256 |
| 250       | 490863   | 11362570 | 10871707 | 0.0432 |
| 300       | 942768.2 | 12219762 | 11276994 | 0.0772 |
| 350       | 1033265  | 12819662 | 11786397 | 0.0806 |

对结果进行对比分析，我们发现车辆规模300辆车后，随着服务车队规模的增加并不能显著提高乘客服务水平并减少TVKT。对于本次平行模拟来说，300辆车时节省了7.72%的路程；而当车队规模增加到350辆时，节省了8.06%的路程，其路程减小率仅仅提高了0.34%。所以我们最终选择300辆车是作为后续模拟分析的最佳的车队规模。

![](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/4-18.png)

<p align="center">图12 订单路程长度分布——一对一出租车模式vs拼车模式</p>

Axhausen等人认为订单的距离[5]满足的分布情况可以从一定程度上看出运营方式的优劣，由上图简单分析，拼车与没有拼车之间对比不是很明显，但是通过细节大体能够分析出来，订单大部分都分布在900m~2100m之间，同时还可以看出拼车的短距离订单相比于没有拼车的短距离订单得到了更多的满足。说明启发插入式拼车算法对于满足短距离需求相比于没有拼车的模式而言有着一定的优势。

Meng Wang等人认为订单等待时间[6]是乘客感知服务水平高低的一个重要指标，订单等待时间是指从发出订单请求后到出租车接到乘客为止的这一段时间长度，通常也就是我们现实生活中的站在指定地点等车来的那段时间，所以对每个订单的等待时间进行分析是非常重要的。

为了是分析结果更具有可视性，我们选择将5分钟也就是300s作为一个分析区间对最终的结果进行可视化分析，最终得到，在车队规模为175辆的情况下，对拼车与不拼车的仿真结果进行数据分析得到各个订单等待时间区间内的订单数量分布如下图：

![](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/4-19.png)

<p align="center">图13 订单等待时间分布情况——一对一出租车模式vs拼车模式</p>

由图分析可知，拼车与一对一的大多是的订单的订单等待时间都在5分钟以内，但是通过拼车使得乘客等待时间得到了显著地减少。从这个角度来看，实施拼车策略确实可以在有限地车队规模下，减少乘客的等待时间，提高乘客的满意度。

![](https://gitee.com/wild_ptr/taxi-sim/raw/master/docs/img/4-20.png)

<p align="center">图14 订单等待时间在5分钟内分布情况——一对一出租车模式vs拼车模式</p>

我们发现，前五分钟内各个时间段内也是拼车模式下的订单数量分布情况比没有拼车模式的分布情况要好很多，绝大多数的订单都集中在前三分钟内满足了，快速满足（等待时间小于1分钟）的订单数量也是增加了616个订单，相比于没有拼单的模式的361个订单，提高了近两倍，这也更加说明了在一定拼车规模下，拼车模式有着快速满足出行需求的优势。

### 参考文献

[1] Krajzewicz, D., Erdmann, J., Behrisch, M., & Bieker, L. (2012). Recent development and applications of SUMO-Simulation of Urban MObility. International journal on advances in systems and measurements, 5(3&4).

[2] Di Maria, A., Araldo, A., Morana, G., & Di Stefano, A. (2018, November). AMoDSim: An efficient and modular simulation framework for autonomous mobility on demand. In International Conference on Internet of Vehicles (pp. 165-178). Springer, Cham.

[3] Chuah, S. P., Xiang, S., & Wu, H. (2018, February). Optimal rebalancing with waiting time constraints for a fleet of connected autonomous taxi. In 2018 IEEE 4th World Forum on Internet of Things (WF-IoT) (pp. 629-634). IEEE.

[4]K.Mattas,M.Makridis,P.Hallac,M.A.Raposo,C.Thiel,T.Toledo,and B.Ciuffo.Simulating deployment of connectivity and automation on the antwerp ring road.IET' Intelligent TransportSystems，12(9):1036-1044，2018.

[5]Kay W Axhausen，Andreas Horni, and Kai Nagel. The multi-agent transport simulation MATSim.Ubiquity Press，2016.

[6]Lin Xiao, Meng Wang, and Bart van Arem.Realistic car-following models for microscopic simula-tion of adaptive and cooperative adaptive cruise control vehicles.Transportation Research Record,2623(1):1-9,2017.
